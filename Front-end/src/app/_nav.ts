export const navItems = [
    {
      name: 'Danh mục',
      url: '/trangchu',
      icon: 'pi pi-tags',
    },
    {
      name: 'Quản lý sản phẩm',
      url: '/quanlysanpham',
      icon: 'pi pi-briefcase'
    }
    ,
    {
      name: 'Quản lý nhân viên',
      url: '/Ecommerce-Website',
      icon: 'fa fa-user'
    },
    {
      name: 'Về chúng tôi',
      url: '/gioithieu',
      icon: 'fa fa-info-circle'
    },
    {
      name: 'Liên hệ',
      url: '/lienhe',
      icon: 'pi pi-comments'
    },
    {
      name: 'FAQ',
      url: '/faq',
      icon: 'fa fa-question-circle'
    }

  ];