import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { WavesModule, ButtonsModule, IconsModule } from 'angular-bootstrap-md'
import { FooterModule } from '@progress/kendo-angular-grid';
import { DataViewModule } from 'primeng/dataview';
import { PaginatorModule } from 'primeng/paginator';
import { GalleriaModule } from 'primeng/galleria';
import { SanPhamRoutingModule } from '../sanpham/sanpham-routing.module';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { SplitButtonModule } from 'primeng/splitbutton';
import { InputTextModule } from 'primeng/inputtext';
import {CarouselModule} from 'primeng/carousel';

@NgModule({
  imports: [
    FormsModule,
    CarouselModule,
    DashboardRoutingModule,
    WavesModule,
    ButtonsModule,
    FooterModule,
    PaginatorModule,
    DataViewModule,
    GalleriaModule,
    DropdownModule,
    ButtonModule,
    SplitButtonModule,
    IconsModule,
    InputTextModule,
    DataViewModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  declarations: [ DashboardComponent ]
})
export class DashboardModule { }
