import { Component, OnInit } from '@angular/core';
import { Danhmuc } from '../models/Danhmuc.class';
import {MessageService} from 'primeng/components/common/messageservice';
import {map} from 'rxjs/operators';
import {SelectItem} from 'primeng/api';
import {Response} from '@angular/http';
import { DanhmucService } from '../shared/Danhmuc.service';
import { SanPhamService } from '../shared/Sanpham.service';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [MessageService]
})
export class DashboardComponent implements OnInit {

  images:any[];
  public loading = false;
  public Danhmucs: Danhmuc[];

  selectedDanhmuc: Danhmuc;
  constructor(private service: DanhmucService,private sanphamService: SanPhamService,private messageService: MessageService){}

  sortOptions: SelectItem[];

  ngOnInit(): void {
    this.loadData();
    this.sortOptions = [ {label: 'Nội dung', value: 'content'}];
  
    this.images = [];
  this.images.push({source:'https://img.atpsoftware.vn/2017/03/quang-cao-zalo23.jpeg', alt:'Description for Image 1', title:'Title 1'});
  this.images.push({source:'https://o.aolcdn.com/images/dims?quality=85&image_uri=https%3A%2F%2Fo.aolcdn.com%2Fimages%2Fdims%3Fcrop%3D7999%252C4500%252C0%252C0%26quality%3D85%26format%3Djpg%26resize%3D1600%252C900%26image_uri%3Dhttps%253A%252F%252Fs.yimg.com%252Fos%252Fcreatr-images%252F2019-02%252Fe796d060-35ee-11e9-a99f-df6db4ff5dcf%26client%3Da1acac3e1b3290917d92%26signature%3D3e3dd579728303bc88d8ccf8c903ca85c472a6be&client=amp-blogside-v2&signature=32881b4d218cb3eeb892f8f1bf77520b05a2ab2c', alt:'Description for Image 2', title:'Title 2'});
  this.images.push({source:'https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/2837182/1160/771/m1/fpnw/wm0/sky_background_with_sun1-.jpg?1497313685&s=202c7f0a5ff106d7d6e05b638390a626', alt:'Description for Image 3', title:'Title 3'});
  this.images.push({source:'https://webdoctor.vn/wp-content/uploads/2018/06/Photoshop.jpg', alt:'Description for Image 4', title:'Title 4'});
  this.images.push({source:'https://about.gitlab.com/images/blogimages/gitlab-blog-cover.png', title:'Title 6'});
  this.images.push({source:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS6_v-OuuaMi8UwMtBdC9aCMc9nTUAx3zKBjRRlxVUeBqxc8PhB', alt:'Description for Image 8', title:'Title 8'});
  this.images.push({source:'http://s1.picswalls.com/wallpapers/2015/12/12/beautiful-background_124413827_294.jpg', alt:'Description for Image 9', title:'Title 9'});
  this.images.push({source:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTuyZNGcz5iiG86qKkiaEQ8yIP4MlaQEg89OVKfbnrlhsAhmhkm', alt:'Description for Image 10', title:'Title 10'});
  }

  loadData(): any {
    this.loading=true;
    this.service.getDanhmuc().pipe(map((data:Response)=>{
      return data.json() as Danhmuc[];
  })).toPromise().then(
    x=>{
      this.Danhmucs = x;
      this.Danhmucs.reverse();
      this.loading = false;
    }
  );
    
  }

  sangtrangSanpham(event:Event,danhmuc: Danhmuc){
    this.sanphamService.DanhmucID = danhmuc.id;
  }

}
