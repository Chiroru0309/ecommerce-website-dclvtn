import { Component, OnInit, AfterViewInit } from '@angular/core';
import { User } from '../models/user.class';
import {map} from 'rxjs/operators';
import { FormBuilder } from '@angular/forms';
import { UserService } from '../shared/User.service';
import { ToastrService } from 'ngx-toastr';
import {MessageService} from 'primeng/components/common/messageservice';
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { process, State } from '@progress/kendo-data-query';

@Component({
  selector: 'app-quanly-khachhang',
  templateUrl: './quanly-khachhang.component.html',
  styleUrls: ['./quanly-khachhang.component.scss'],
  providers: [MessageService]
})
export class QuanlyKhachhangComponent implements OnInit {
  public gridData: GridDataResult;
  public loading = false;
  public Users: User[];
  public data: Object[];
  public selectedUser: User ={
    id:0,
    name:'',
    cityId:0,
    cityName: ''
  }
  public state: State = {
    skip: 0,
    take: 5,
  };
  opened:boolean = false;
  dialogOpened:boolean = false;
  images: any[];
  id:number = 0;
  name:string = '';
  cityId:number = 0;
  cityName:string = '';
  displayThemmoiDialog: boolean = false;
  themmoiUser:boolean = false;
  
  constructor( private service: UserService, private messageService: MessageService) { }

  ngOnInit() {
  this.Loading();
  
  let a:User[] = this.Users;

  this.images = [];
  this.images.push({source:'https://img.atpsoftware.vn/2017/03/quang-cao-zalo23.jpeg', alt:'Description for Image 1', title:'Title 1'});
  this.images.push({source:'https://o.aolcdn.com/images/dims?quality=85&image_uri=https%3A%2F%2Fo.aolcdn.com%2Fimages%2Fdims%3Fcrop%3D7999%252C4500%252C0%252C0%26quality%3D85%26format%3Djpg%26resize%3D1600%252C900%26image_uri%3Dhttps%253A%252F%252Fs.yimg.com%252Fos%252Fcreatr-images%252F2019-02%252Fe796d060-35ee-11e9-a99f-df6db4ff5dcf%26client%3Da1acac3e1b3290917d92%26signature%3D3e3dd579728303bc88d8ccf8c903ca85c472a6be&client=amp-blogside-v2&signature=32881b4d218cb3eeb892f8f1bf77520b05a2ab2c', alt:'Description for Image 2', title:'Title 2'});
  this.images.push({source:'https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/2837182/1160/771/m1/fpnw/wm0/sky_background_with_sun1-.jpg?1497313685&s=202c7f0a5ff106d7d6e05b638390a626', alt:'Description for Image 3', title:'Title 3'});
  this.images.push({source:'https://webdoctor.vn/wp-content/uploads/2018/06/Photoshop.jpg', alt:'Description for Image 4', title:'Title 4'});
  this.images.push({source:'https://images.gog.com/bbba2ce5901fe222dc2493997192189215e5c8e3b576dc37ed1c1023764ee995.jpg', title:'Title 5'});
  this.images.push({source:'https://about.gitlab.com/images/blogimages/gitlab-blog-cover.png', title:'Title 6'});
  this.images.push({source:'https://wallpapersite.com/images/wallpapers/mileena-3840x2160-mortal-kombat-x-pc-games-xbox-one-ps4-26.jpg', alt:'Description for Image 7', title:'Title 7'});
  this.images.push({source:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS6_v-OuuaMi8UwMtBdC9aCMc9nTUAx3zKBjRRlxVUeBqxc8PhB', alt:'Description for Image 8', title:'Title 8'});
  this.images.push({source:'http://s1.picswalls.com/wallpapers/2015/12/12/beautiful-background_124413827_294.jpg', alt:'Description for Image 9', title:'Title 9'});
  this.images.push({source:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTuyZNGcz5iiG86qKkiaEQ8yIP4MlaQEg89OVKfbnrlhsAhmhkm', alt:'Description for Image 10', title:'Title 10'});
  }

  Loading():any{
    this.loading = true;
    this.service.getUsers()
    .pipe(map( data =>
    {
        return data.json() as User[]; 
      })).toPromise().then(
        x=>{
          this.Users = x;
          this.Users.reverse();
          this.gridData =  process(this.Users, this.state);
          this.loading = false;
        }
      )
  }

  public close(status) {
    this.dialogOpened = false;
    this.opened = false;
  }

  public closeThemmoi(status)
  {
    this.displayThemmoiDialog = false;
  }

  onChange(value?: Date): void {
    // this.birth = value? value:null;
    //this.birth= value ? `${new Intl.DateTimeFormat('en-US').format(value)}` : '';
  } 

  public open(usr:User) {
    // this.selectedUser = usr;
    // this.dialogOpened = true;
    // this.opened = true;
  }

  public openThemmoi(usr:User){
    // this.themmoiUser = false;
    // this.displayThemmoiDialog = true;
    // this.selectedUser = usr;
    // this.id = this.selectedUser.id;
    // this.name = this.selectedUser.name;
    // this.cityId = this.selectedUser.cityId;
    // this.cityName = this.selectedUser.cityName;
  }
  
  public deleteUser()
  {
    this.service.deleteUsers(this.selectedUser).subscribe((data:User)=>
    {
        this.messageService.add({severity:'error',summary:'Xoá thành công', detail:'Bạn đã xóa tài khoản ' + this.selectedUser.name})
        this.Loading();
        this.selectedUser = null;
    });
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state=state;
    //reload dữ liệu
    this.Loading();
  }

  Themmoisanpham(event:Event){
    this.id = 0;
    this.name = '';
    this.cityName = '';
    this.cityId = 0;
    this.themmoiUser = true;
    this.selectedUser = null;
    // this.displayThemmoiDialog = true;
    event.preventDefault();
  }


  ResetDulieu()
  {
    this.selectedUser = null;
    this.themmoiUser = false;
  }
  
  OnSubmit()
  {
    
    if (this.id !== null && this.name !== '')
    {
      if (this.themmoiUser)
      {
        this.selectedUser ={
          id: this.id,
          name: this.name,
          cityId: this.cityId,
          cityName: this.cityName
        };
        this.service.postUsers(this.selectedUser).subscribe((data:User)=>
        {
          this.messageService.add({severity:'success',summary:'Thêm mới thành công !', detail: 'Bạn đã thêm mới nhân viên' + this.selectedUser.name});
          this.Loading();
          this.id=0;
          this.name='';
          this.cityId = 0;
          this.cityName = '';
         
        })
      }
      else
      {
        this.selectedUser ={
          id:this.id,
          name: this.name,
          cityId: this.cityId,
          cityName: this.cityName
        };
        this.service.putUsers(this.selectedUser).subscribe( (data:User)=>
        {
          this.messageService.add({severity:'info', summary:'Thay đổi thành công' ,detail:'Bạn đã thay đổi thông tin phòng ban' + this.selectedUser.name});
          this.Loading();
          this.id=0;
          this.name='';
          this.cityId=0;
          this.cityName='';
          
        })
      }
    }
    else
    {
      this.messageService.add({severity:'warn', summary: 'Không thể cập nhật thông tin', detail:'Thông tin phòng ban không thể để trống !'});
      this.Loading();
          this.id=0;
          this.name='';
          this.cityId=0;
          this.cityName='';
    }
  }

}
