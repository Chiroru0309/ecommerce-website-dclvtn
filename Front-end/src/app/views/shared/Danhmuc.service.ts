import { Injectable } from '@angular/core';
import {Http, Response,  Headers, RequestOptions, RequestMethod} from '@angular/http';
import {sanpham} from '../models/sanpham.class';
import { Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpHeaders} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
  })

export class DanhmucService{
    API_URL: string = 'https://ws-bkecommerce2020.herokuapp.com/api-category';
    constructor( public http: Http) { }
    getDanhmuc() {
        return this.http.get(this.API_URL);
  }
}