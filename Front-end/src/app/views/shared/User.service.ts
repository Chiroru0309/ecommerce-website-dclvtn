import { Injectable } from '@angular/core';
import {Http, Response,  Headers, RequestOptions, RequestMethod} from '@angular/http';
import {User} from '../models/user.class';
import { Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpHeaders,HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class UserService{
    constructor(public http: Http){}
    getUsers(){
        let headers = new Headers({'Content-Type':'application/json'});
        let options = new RequestOptions({headers:headers});
        return this.http.get('https://ws-bkecommerce2020.herokuapp.com//api-district/1',options);
    }

    postUsers(usr:User):Observable<User>{
        let headers = new Headers({'Content-Type':'application/json'});
        let options = new RequestOptions({headers:headers});
        return this.http.post('http://5d4a5a4f5c331e00148eb0bb.mockapi.io/employee/',options)
        .pipe(map(
            (
              res:Response)=> res.json()
            ));
    }

    putUsers(usr:User):Observable<User>{
        let headers = new Headers({'Content-Type':'application/json'});
        let options = new RequestOptions({headers:headers});
        return this.http.put('http://5d4a5a4f5c331e00148eb0bb.mockapi.io/employee/'+usr.id,usr,options)
        .pipe(map(
            (
              res:Response)=> res.json()
            ));
    }

    deleteUsers(usr:User):Observable<User>
    {
        let headers = new Headers({'Content-Type':'application/json'});
        let options = new RequestOptions({headers:headers});
        return this.http.delete('http://5d4a5a4f5c331e00148eb0bb.mockapi.io/employee/'+usr.id)
        .pipe(map(
            (
              res:Response)=> res.json()
            ));
    }

}