import { Injectable } from '@angular/core';
import {Http, Response,  Headers, RequestOptions, RequestMethod} from '@angular/http';
import {sanpham} from '../models/sanpham.class';
import { Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpHeaders} from '@angular/common/http';
import { Options } from 'selenium-webdriver/firefox';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};



@Injectable({
  providedIn: 'root'
})
export class SanPhamService {
  DanhmucID:number;
  SanphamchitietID : string;
  API_URL: string = 'https://ws-bkecommerce2020.herokuapp.com/api-product?categoryId=';
  constructor( public http: Http) { }
  getsanphamsSmall(danhmucid: number) {
    if (danhmucid != null)
    return this.http.get(this.API_URL + danhmucid.toString());
    else return;
  }

  getsanphamtheoID(sanphamselectedID:string){
   return this.http.get('https://ws-bkecommerce2020.herokuapp.com/api-product/'+sanphamselectedID);
  }

  putsanphams(sp: sanpham): Observable<sanpham>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
   return this.http.put('http://5d4a5a4f5c331e00148eb0bb.mockapi.io/department/' + sp.id,sp,options).pipe(map((res: Response) => res.json()));
  }

  postsanphams(sp: sanpham): Observable<sanpham>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post('http://5d4a5a4f5c331e00148eb0bb.mockapi.io/department',sp,options)
    .pipe(map(
      (
        res:Response)=> res.json()
      ));
  }

  deletesanphams(sp:sanpham): Observable<sanpham>{
    let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.delete('http://5d4a5a4f5c331e00148eb0bb.mockapi.io/department/' + sp.id,options)
    .pipe(map(
      (
        res:Response)=> res.json()
      ));
  }
}
