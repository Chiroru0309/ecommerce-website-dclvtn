import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import 'rxjs/Rx';
import {Router} from '@angular/router';
//import {SESSION_STORAGE, StorageService} from 'angular-webstorage-service';
import { Inject, Injectable } from '@angular/core';


@Component({
  selector: 'app-dashboard',
  templateUrl: './login.component.html',
  styleUrls: ['../register/register.component.scss']
})
export class LoginComponent implements OnInit{ 
	base_link = 'http://www.localhost/Software-sharing-website/Back-End/index.php';
	login_form : FormGroup;
	username : FormControl;
	password : FormControl;
	constructor(private http: Http, private router : Router){
		this.username =  new FormControl("",Validators.compose([
			Validators.required,
			Validators.minLength(3),
			Validators.maxLength(255)
		  ]));
		this.password =  new FormControl("",Validators.compose([
			Validators.required,
			Validators.minLength(3),
			Validators.maxLength(255)
		]));
		this.login_form = new FormGroup({
			Username : this.username,
			Pass_word : this.password
		});
	}
	ngOnInit() {}
	login(){
		if (this.login_form.valid) {
			console.log('abc');
			this.http.post(this.base_link + '?controller=Login&action=login',this.login_form.value)
			.map(response => {return response.json()} ).subscribe(
				data => {
					console.log(data);
            		if (data.success == "1"){
						//this.storage.set('login',data.mess);
            			this.router.navigateByUrl('/trangchu');
            		}
            		else
            			alert(data.mess);
          		},
         		err => {
            		console.log(err);
          		}
			);
		}
		else alert("Vui lòng điền đúng định dạng thông tin");
	}
}
