import { Component, OnInit, AfterViewInit,TemplateRef } from '@angular/core';

//import service
import {SanPhamService} from '../shared/Sanpham.service';

//import toastr
import { ToastrService } from 'ngx-toastr';

//import ngx-modal
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {map} from 'rxjs/operators';
import {Response} from '@angular/http';
import {sanpham} from '../models/sanpham.class';
import {SelectItem} from 'primeng/api';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { BinhLuanService } from '../shared/BinhLuan.service';
import { UserService } from '../shared/User.service';
import { BinhLuan } from '../models/BinhLuan.class';
import { User } from '../models/user.class';
@Component({
  selector: 'app-sanpham',
  templateUrl: './sanpham.component.html',
  styleUrls: ['./sanpham.component.scss'],
  providers: [MessageService]
})
export class SanPhamComponent implements OnInit, AfterViewInit {
  public loading = false;
  public sanphams: sanpham[];
  public binhluans: BinhLuan[];
  public users: User[];
  selectedsanpham: sanpham;
    
  displayDialog: boolean;
  displayDownloadDialog: boolean;
  sortOptions: SelectItem[];

    msgs: Message[] = [];
    
    sortKey: string;

    sortField: string;

    sortOrder: number;

    displayThemmoiDialog: boolean = false;

    themmoiSanPham: boolean = false;

    id:String = '';
    name:String = '';
    Tentouch = false;
    IDtouch = false;

  constructor(private service: SanPhamService, private binhluanService: BinhLuanService,private userService:UserService,private toastr: ToastrService, private messageService: MessageService ) { }
  images: any[];
  ngOnInit() {
    this.loadData();
    this.sortOptions = [
        {label: 'Tên', value: 'name'},
        {label: 'Nội dung', value: 'content'},
        {label: 'Mô tả', value: 'hortDescription'}
     ];
  this.images = [];
  this.images.push({source:'https://img.atpsoftware.vn/2017/03/quang-cao-zalo23.jpeg', alt:'Description for Image 1', title:'Title 1'});
  this.images.push({source:'https://o.aolcdn.com/images/dims?quality=85&image_uri=https%3A%2F%2Fo.aolcdn.com%2Fimages%2Fdims%3Fcrop%3D7999%252C4500%252C0%252C0%26quality%3D85%26format%3Djpg%26resize%3D1600%252C900%26image_uri%3Dhttps%253A%252F%252Fs.yimg.com%252Fos%252Fcreatr-images%252F2019-02%252Fe796d060-35ee-11e9-a99f-df6db4ff5dcf%26client%3Da1acac3e1b3290917d92%26signature%3D3e3dd579728303bc88d8ccf8c903ca85c472a6be&client=amp-blogside-v2&signature=32881b4d218cb3eeb892f8f1bf77520b05a2ab2c', alt:'Description for Image 2', title:'Title 2'});
  this.images.push({source:'https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/2837182/1160/771/m1/fpnw/wm0/sky_background_with_sun1-.jpg?1497313685&s=202c7f0a5ff106d7d6e05b638390a626', alt:'Description for Image 3', title:'Title 3'});
  this.images.push({source:'https://webdoctor.vn/wp-content/uploads/2018/06/Photoshop.jpg', alt:'Description for Image 4', title:'Title 4'});
  this.images.push({source:'https://about.gitlab.com/images/blogimages/gitlab-blog-cover.png', title:'Title 6'});
  this.images.push({source:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS6_v-OuuaMi8UwMtBdC9aCMc9nTUAx3zKBjRRlxVUeBqxc8PhB', alt:'Description for Image 8', title:'Title 8'});
  this.images.push({source:'http://s1.picswalls.com/wallpapers/2015/12/12/beautiful-background_124413827_294.jpg', alt:'Description for Image 9', title:'Title 9'});
  this.images.push({source:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTuyZNGcz5iiG86qKkiaEQ8yIP4MlaQEg89OVKfbnrlhsAhmhkm', alt:'Description for Image 10', title:'Title 10'});
  }
  

  public ngAfterViewInit(): void {
    //mở rộng 1 hàng
    //this.grid.expandRow(0);
  }

  
  //thay đổi dữ liệu khi chuyển trang

  //load dữ liệu 
  loadData(): any {
    this.loading=true;
    this.service.getsanphamsSmall(this.service.DanhmucID).pipe(map((data:Response)=>{
        return data.json() as sanpham[];
    })).toPromise().then(
      x=>{
        this.sanphams = x;
        this.sanphams.reverse();
        this.loading = false;
      }
    );
    // this.binhluanService.getBinhLuans().pipe(map((data:Response)=>{
    //   return data.json() as BinhLuan[];
    // })).toPromise().then(
    //   x=>{
    //     this.binhluans = x;
    //     this.binhluans.reverse();
    //   }
    // );
    // this.userService.getUsers().pipe(map((data:Response)=>{
    //   return data.json() as User[];
    // })).toPromise().then(
    //   x=>{
    //     this.users = x;
    //     this.users.reverse();
    //   }
    // )
  }

  IDTouch()
  { 
    // this.IDtouch = true;
  }

  TenTouch()
  {
    // this.Tentouch = true;
  }

  Themmoisanpham(event:Event){
    // this.id = '';
    // this.name = '';
    // this.themmoiSanPham = true;
    // this.selectedsanpham = null;
    // this.displayThemmoiDialog = true;
    // event.preventDefault();
  }


  ResetDulieu()
  {
    // this.selectedsanpham = null;
    // this.themmoiSanPham = false;
    // this.Tentouch = false;
    // this.IDtouch = false;
  }

  selectsanpham(event: Event, sanpham: sanpham) {
    // this.selectedsanpham = sanpham;
    // this.displayDialog = true;
    // event.preventDefault();
  }

  handleClick(event:Event, sanpham: sanpham)
  {
    // this.selectedsanpham = sanpham;
    // this.displayDownloadDialog = true;
    // event.preventDefault();
  }

  onDialogHide() {
      // this.selectedsanpham = null;
  }
    
  showSuccess() {
    // // this.selectedsanpham.Soluottai++;
    // this.service.putsanphams(this.selectedsanpham).subscribe((data:sanpham)=>{
    //   this.messageService.add({severity:'success', summary: 'Chỉnh sửa thành công', detail:'Bạn đã chỉnh sửa ' + this.selectedsanpham.name});
    //   this.loadData();
    // })
    
}

selectsanphamAdmin(event: Event,sp:sanpham) {
    // //this.Khoitao();
    // this.selectedsanpham = sp;
    // this.id = this.selectedsanpham.id;
    // this.name = this.selectedsanpham.name;
    // this.displayThemmoiDialog = true;
    // event.preventDefault();
  }

  Onsubmit()
  {
    // if (this.id !== '' && this.name !== '')
    // {
    //   if (this.themmoiSanPham)
    //   {
    //     this.selectedsanpham ={
    //       id: this.id,
    //       name: this.name
    //     };
    //     this.service.postsanphams(this.selectedsanpham).subscribe((data:sanpham)=>
    //     {
    //       this.messageService.add({severity:'success',summary:'Thêm mới thành công !', detail: 'Bạn đã thêm mới phòng ban' + this.selectedsanpham.name});
    //       this.loadData();
    //       this.id = '';
    //       this.name = '';
         
    //     })
    //   }
    //   else
    //   {
    //     this.selectedsanpham ={
    //       id: this.id,
    //       name: this.name
    //     };
    //     this.service.putsanphams(this.selectedsanpham).subscribe( (data:sanpham)=>
    //     {
    //       this.messageService.add({severity:'info', summary:'Thay đổi thành công' ,detail:'Bạn đã thay đổi thông tin phòng ban' + this.selectedsanpham.name});
    //       this.loadData();
    //       this.id = '';
    //       this.name = '';
    //     })
    //   }
    // }
    // else
    // {
    //   this.messageService.add({severity:'warn', summary: 'Không thể cập nhật thông tin', detail:'Thông tin phòng ban không thể để trống !'});
    //   this.loadData();
    //   this.id = '';
    //   this.name = '';
    // }
  }

  // Xoasanpham(sp:sanpham)
  // {
  //   this.service.deletesanphams(sp).subscribe((data:sanpham)=>{
  //     this.messageService.add({severity:'info', summary:'Xóa thành công' ,detail:'Bạn đã xóa thông tin phòng ban' + this.selectedsanpham.name});
  //         this.loadData();
  //         this.id = '';
  //         this.name = '';
  //   })

  // }

  sangtrangchitietSanpham(event : Event, sanphamduocchon :sanpham)
  {
      this.service.SanphamchitietID = sanphamduocchon.id;
  }


onSortChange(event) {
  let value = event.value;

  if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
  }
  else {
      this.sortOrder = 1;
      this.sortField = value;
  }
}
}
