export class sanpham {
    id: string;
    name: string;
    brandId: number;
    categoryId: number;
    content: string;
    shortDescription: string;
    unitId: number;
}