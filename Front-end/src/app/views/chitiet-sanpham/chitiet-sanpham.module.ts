import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { WavesModule } from 'angular-bootstrap-md';
import { FooterModule } from '@progress/kendo-angular-grid';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { PaginatorModule } from 'primeng/paginator';
import { DataViewModule } from 'primeng/dataview';
import { GalleriaModule } from 'primeng/galleria';
import { DropdownModule } from 'primeng/dropdown';
import { SplitButtonModule } from 'primeng/splitbutton';
import { InputTextModule } from 'primeng/inputtext';
import { ChitietSanphamComponent } from './chitiet-sanpham.component';
import { ChitietSanphamRoutingModule } from './chitiet-sanpham-routing.module';

@NgModule({
    imports:[
        FormsModule,
        WavesModule,
        FooterModule,
        ButtonModule,
        PaginatorModule,
        DataViewModule,
        GalleriaModule,
        DropdownModule,
        SplitButtonModule,
        InputTextModule,
        DataViewModule,
        ChitietSanphamRoutingModule

    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations: [ChitietSanphamComponent]
})
export class ChitietSanphamModule{
    
}