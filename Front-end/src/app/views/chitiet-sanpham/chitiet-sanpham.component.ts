import { Component, OnInit } from '@angular/core';
import {MessageService} from 'primeng/components/common/messageservice';
import { SanPhamService } from '../shared/Sanpham.service';
import { sanpham } from '../models/sanpham.class';
import {Response} from '@angular/http';
import {map} from 'rxjs/operators';;

@Component({
  selector: 'app-chitiet-sanpham',
  templateUrl: './chitiet-sanpham.component.html',
  styleUrls: ['../../../../node_modules/bootstrap/dist/css/bootstrap.min.css'],
  providers: [MessageService]
})
export class ChitietSanphamComponent implements OnInit {

  constructor( private service: SanPhamService, private messageService: MessageService) { }

  public loading = false;
  public sanpham: sanpham;
  selectedsanpham: sanpham;


  ngOnInit() {
    this.loadData();
  }


  loadData(): any {
    this.loading=true;
    this.service.getsanphamtheoID(this.service.SanphamchitietID).pipe(map((data:Response)=>{
        return data.json() as sanpham;
    })).toPromise().then(
      x=>{
        this.sanpham = x;
      }
    );
  }
}
