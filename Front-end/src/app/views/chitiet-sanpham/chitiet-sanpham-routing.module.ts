import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';
import { ChitietSanphamComponent } from './chitiet-sanpham.component';



const routes: Routes = [
  {
    path: '',
    component: ChitietSanphamComponent,
    data: {
      title: 'Chi tiết sản phẩm'
    }
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChitietSanphamRoutingModule {}
